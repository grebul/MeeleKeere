function main() {
    var level = 0;
    var rowNr = level * 2 + 4;
    var columnNr = level * 3 + 6;
    var letterMatrix = [];
    var table;
    var score = 0;
    var totalScore = 0;
    $('.current-score p').text(score);
    $('.total-score p').text(totalScore);
    var currTime = new Date().getTime();
    var newTime;
    var happyFace = false;
    $('.levels').hide();
    $('#level-0').css('color', 'black');
    var level0 = true;
    var level1 = false;
    var level2 = false;
    var level3 = false;
    var level4 = false;
    var level5 = false;
    var level6 = false;
    var startingGame = true;

    // A function to create a table with specific dimensions and content
  function createTable(tableData) {
    table = document.createElement('table');
    var tableBody = document.createElement('tbody');
    tableData.forEach(function(rowData) {
      var row = document.createElement('tr');
      rowData.forEach(function(cellData) {
        var cell = document.createElement('td');
        cell.appendChild(document.createTextNode(cellData));
        row.appendChild(cell);
      });
      tableBody.appendChild(row);
    });
    table.appendChild(tableBody);
    $(".gameboard").append(table);
  }

//Dictionary of words in the format of Object notation === JSON
  var positiveWords = {
    3: ['HEA', 'AUS', 'EDU', 'IME', 'SOE', 'ILU', 'ÕNN', 'ABI', 'ÄGE'],
    4: ['TORE', 'ILUS', 'MUSI', 'USIN', 'ÜLIM', 'LEID', 'ANNE', 'LAHE', 'NAER', 'TARK', 'HOOL', 'MÕNU', 'MUHE', 'KENA', 'RÕÕM', 'MÄNG', 'TAIP', 'HUVI', 'ÕDUS', 'VÕLU'],
    5: ['ARMAS', 'PARIM', 'TUGEV', 'LÕBUS', 'VAHVA', 'NUNNU', 'TUBLI', 'MAGUS', 'SUPER', 'SÄRAV', 'SPORT', 'UUDNE', 'TEGUS', 'TÄIUS', 'MÕNUS', 'PÕNEV', 'VÕLUV'],
    6: ['HOOLIV', 'SÕPRUS', 'USINUS', 'KALLIS', 'ISUKAS', 'VALMIS', 'HOOLAS', 'ARUKAS', 'HUUMOR', 'EDUKAS', 'TARKUS', 'MÕTLIK', 'SOOJUS', 'KAUNIS', 'SIIRAS', 'SIIRUS', 'RÕÕMUS'],
    7: ['KULLAKE', 'TUGEVUS', 'MEELDIV', 'STIILNE', 'MÕISTEV', 'VALLATU', 'ANDEKAS', 'JUTUKAS', 'ASJALIK', 'ILMEKAS', 'NUTIKAS', 'ÜLLATUS', 'ERILINE', 'VAJALIK', 'IMELINE', 'SEIKLUS',
      'ÕNNELIK', 'MÕISTUS', 'VÄÄRTUS', 'ABISTAV', 'USALDUS', 'HUVITAV'],
    8: ['ARMASTUS', 'HINNATUD', 'VIISAKAS', 'SÕBRALIK', 'VALLATUS', 'NALJAKAS', 'AKTIIVNE', 'SPORTLIK', 'VAIMUKAS', 'VÄÄRIKAS', 'VÄÄRIKUS', 'HOOLIKAS', 'TÄIUSLIK', 'MÕISTLIK', 'TAIBUKAS', 'SÜDAMLIK'],
    9: ['ELEGANTNE', 'ABIVALMIS', 'HINDAMATU'],
    10: ['ARMASTATUD', 'POSITIIVNE', 'OTSEKOHENE', 'NAERUSUINE', 'ASENDAMATU', 'AINULAADNE', 'VÄÄRTUSLIK'],
    11: ['OPTIMISTLIK', 'MITMEKÜLGNE', 'ENESEKINDEL'],
    12: ['KIRJELDAMATU'],
    13: ['USALDUSVÄÄRNE', 'TÄHELEPANELIK', 'IHALDUSVÄÄRNE'],
    14: ['VASTUPANDAMATU'],
    15: ['SEIKLUSHIMULINE']
  };

//Two dimensional array of words that are put on the gameBoard, 0 position is
//the word and next positions are start site and end site on the gameBoard.
  var usedWords = [];

//Estonian alphabet to fill in empty spaces.
  var alphabet = ['A', 'B', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
                'P', 'R', 'S', 'T', 'U', 'V', 'Õ', 'Ä', 'Ö', 'Ü'];

//Function to fill in the whole gameboard with letters.
  function fillGameMatrix() {
        var chancesToTry = 100;  // how many unsuccessful additions can be made before aborting the fill of game board
        while (chancesToTry > 0) {
            // first we will assign the start position and direction for the new word to be added
            var rowIndicator = Math.floor(Math.random() * rowNr);
            var columnIndicator = Math.floor(Math.random() * columnNr);
            var direction = Math.floor(Math.random() * 4) + 1;  // direction of reading the word
            // 1 = right, 2 = left, 3 = down, 4 = up
            var distance = findDistance(rowIndicator, columnIndicator, direction);
            if (distance > 0) {
                var added = addWord(rowIndicator, columnIndicator, direction, distance);
                if (!added) {
                    chancesToTry -= 1;
                }
            }
        }
        fillEmptySpaces();
    }

//Finds the distance to the end of the game board from the designated position.
  function findDistance(rowIndicator, columnIndicator, direction) {
          // direction: 1 = right, 2 = left, 3 = down, 4 = up
          var distance = 0;
          if (direction == 1) {
              distance += rowNr - rowIndicator;
          } else if (direction == 3) {
              distance += columnNr - columnIndicator;
          } else if (direction == 2) {
              distance += rowIndicator + 1;
          } else if (direction == 4) {
              distance += columnIndicator + 1;
          }
          return distance;
      }

//Fills empty spaces on the game board after the words have been added.
   function fillEmptySpaces() {
       for (var i = 0; i < rowNr; i++) {
           for (var j = 0; j < columnNr; j++) {
             if (letterMatrix[i] === undefined || letterMatrix[i][j] === undefined) {
               assignCharactersToMatrix(i, j, alphabet[Math.floor(Math.random() * alphabet.length)]);
             }
           }
       }
   }

//Adds a letter to the gameBoard.
   function assignCharactersToMatrix(rowIndicator, columnIndicator, charToBeAssigned) {
     if (letterMatrix[rowIndicator] === undefined) {
       letterMatrix[rowIndicator] = [];
     }
       letterMatrix[rowIndicator][columnIndicator] = charToBeAssigned;
   }

//Adds one word to the gameBoard and returns true if it was successful.
   function addWord(rowIndicator, columnIndicator, direction, distance) {
        // direction: 1 = right, 2 = left, 3 = down, 4 = up
        var added = false;
        // access to wordList by key (that is word length)
        Loop1:
        for (var key = distance; key > 0; key--) {
            // first shuffles the words of length key, so that it wouldn't start with the same word each time
            if (positiveWords.hasOwnProperty(key)) {
                shuffle(positiveWords[key]);
                // then it starts to try each word to fit into the game board
                Loop2:
                for (var wordIndex = 0; wordIndex < positiveWords[key].length; wordIndex++) {
                    // check that this word is not on the game board already,
                    //if is, skip this word in loop1
                    if (usedWords.length > 0) {
                      for (var arrayIndex = 0; arrayIndex < usedWords.length; arrayIndex++){
                        if (usedWords[arrayIndex][0] === positiveWords[key][wordIndex]) {
                          continue Loop1;
                        }
                      }
                    }
                    // next, we define x and y coordinates on the game board that is currently under focus
                    var x = rowIndicator;
                    var y = columnIndicator;
                    var badWord = false;
                    //then it takes each word by each letter
                    Loop3:
                    for (var charIndex = 0; charIndex < key; charIndex++) {
                        // to compare the letter with the position in game board matrix, we have to access the game board.
                        // we take into account the direction of reading the word
                        if (letterMatrix[x] !== undefined && letterMatrix[x][y] !== undefined && (letterMatrix[x][y] !== positiveWords[key][wordIndex][charIndex])) {
                            badWord = true;
                            x = rowIndicator;
                            y = columnIndicator;
                            continue Loop1;
                        }
                        if (charIndex !== key - 1) {
                            if (direction === 1) {
                                x += 1;
                            } else if (direction === 3) {
                                y += 1;
                            } else if (direction === 2) {
                                x -= 1;
                            } else if (direction === 4) {
                                y -= 1;
                            }
                        }
                    }
                    if (!badWord) {
                        added = true;;
                        // now store this word with its start and end coordinates to usedWords array.
                        usedWords.push([positiveWords[key][wordIndex], rowIndicator, columnIndicator, x, y, key]);
                        // another round of going through the same word, to store it on the game board
                        // first, we return the coordinates on the game board back to original
                        x = rowIndicator;
                        y = columnIndicator;
                        for (var charIndex = 0; charIndex < key; charIndex++) {
                            // we take into account the direction of reading the word
                            assignCharactersToMatrix(x, y, positiveWords[key][wordIndex][charIndex]);
                            if (direction === 1) {
                                x += 1;
                            } else if (direction === 3) {
                                y += 1;
                            } else if (direction === 2) {
                                x -= 1;
                            } else if (direction === 4) {
                                y -= 1;
                            }
                        }
                    }
                }
            }
        }
        return added;
    }

//Shuffles the elements of an array a.
    function shuffle(a) {
        var j, x, i;
        for (i = a.length; i; i--) {
            j = Math.floor(Math.random() * i);
            x = a[i - 1];
            a[i - 1] = a[j];
            a[j] = x;
        }
    }

//Adds hints to the hints div.
    function displayHints() {
      for (var i = 0; i < usedWords.length; i++) {
        var word = usedWords[i][0];
        $(".hints p").append("<span ID='" + word + "'> " + word + " </span");
      }
    }


  //Variables for mouse and touch events for selecting letters on the gameboard.
  var startColIndex;
  var startRowIndex;
  var currColIndex;
  var currRowIndex;
  var endColIndex;
  var endRowIndex;

  //Function that starts everything else
  function initialize() {
    $("#final-score").text(""); //eemaldab üleliigse teksti
    if (startingGame) {
      $('#good-job').hide();
      $('#final-score').append("Leia mängulaualt positiivse tähendusega sõnu ning märgista need hiirega lohistades sõna algusest lõpuni. Sõnad paiknevad horisontaalselt või vertikaalselt. Vihjeid võid leida mängualaua all olevast listist. Mida kiiremini sõnu märgistad, seda rohkem punkte saad.")
      $('.end button').text("Alusta");
    } else {
      $('.end').hide(); //hides the message for end-level greeting
    }
    rowNr = level * 2 + 4;  //how many rows
    columnNr = level * 3 + 6;  //how many columns
    score = 0;  //score back to zero
    $('.current-score p').text(score); //näitab teksti
    $('table').remove(); //võtab eelmise leveli tabeli ära
    $('.hints p').text(""); //eemaldab eelmise leveli vihjed
    usedWords = [];
    letterMatrix = []; //teeb mänguvälja tühjaks
    fillGameMatrix(); //teeb uue mänguvälja
    displayHints(); //lisab uued vihjed
    createTable(letterMatrix); //loob visuaalse mänguvälja tabelina

//Effects of different levels
    //change the size and style of letters according to level
    //display flying faces
    if (level === 0) { //the first level
      $('td').css('font-size', '36px');
      $('.hints').css('max-width', '600px');
      $('.end button').show();
    } else if (level === 1) {
      $('td').css('font-size', '30px');
      $('.hints').css('max-width', '600px');
      $('.end button').show();
      flyFace(8000);  // makes first face fly
      $('#level-1').css('color', 'black');
      level1 = true;
    } else if (level === 2) {
      $('td').css('font-size', '26px');
      $('.hints').css('max-width', '600px');
      $('.end button').show();
      $('#level-2').css('color', 'black');
      level2 = true;
    } else if (level === 3) {
      $('td').css('font-size', '24px');
      $('.hints').css('max-width', '600px');
      $('.end button').show();
      $('#level-3').css('color', 'black');
      level3 = true;
    } else if (level === 4) {
      $('td').css('font-size', '19px');
      $('td').css('font-family', 'Arial');
      $('.hints').css('max-width', '600px');
      $('.end button').show();
      $('#level-4').css('color', 'black');
      level4 = true;
    } else if (level === 5) {
      $('td').css('font-size', '18px');
      $('td').css('font-family', 'Arial');
      $('td').css('border-style', 'none');
      $('.hints').css('max-width', '900px');
      $('.end button').show();
      $('#level-5').css('color', 'black');
      level5 = true;
    } else {
      $('td').css('font-size', '16px');
      $('td').css('font-family', 'Arial');
      $('td').css('border-style', 'none');
      $('.hints').css('max-width', '900px');
      $('.end button').hide();
      $('#level-6').css('color', 'black');
      level6 = true;
    }

    if (level !== 0) {
      for (var i = 0; i < (58 + level * level); i++) {
        var newTime = Math.floor(Math.random() * 15 * 60 * 1000) + 10000;  //random time to display the next face image
        //first 10 seconds have no faces and the max time of faces displaying is 15 min and 10 seconds
        flyFace(newTime, "pic" + i);
      }
    }


    //Mouse or touch events for marking words by the user.
    //1) if mouse button is pressed down or screen is touched, the start
    //coordinates are stored and this cell is highlighted
    $('td').on('mousedown touchstart', function(e) {
      e.preventDefault();
      $(this).addClass('marked');
      startColIndex = $(this).closest("td").index();
      startRowIndex = $(this).closest("tr").index();
      //$(this).css("background-color", "rgba(90, 8, 127, 0.5)");

      //during dragging, the current coordinates are stored and the path to start is marked
      $('td').on('mouseover', function() {
        currColIndex = $(this).closest("td").index();
        currRowIndex = $(this).closest("tr").index();
        markCorrectCells();
      });

      //during dragging with finger, the same happens
      $('td').on('touchmove', function(e) {
        $('td').removeClass('marked');
        e.preventDefault();  //et ei skrolliks
        var myLocation = e.originalEvent.changedTouches[0];
        var realTarget = document.elementFromPoint(myLocation.clientX, myLocation.clientY);  //finds the object over which the finger is currently
        currColIndex = $(realTarget).parent().children().index($(realTarget));
        currRowIndex = $(realTarget).parent().parent().children().index($(realTarget).parent());
        markCorrectCells();
      });
    });

    //when leaving a cell, all drag marks are removed
    $('td').on('mouseleave touchleave', function() {
      $('td').removeClass('marked');
    });

    //when mouse button is released, the end coordinates are stored
    $('td').on('mouseup touchend', function() {
      //no more marking
      $('td').off('mouseover touchmove');
      //check if a correct word was marked
      for (var i = 0; i < usedWords.length; i++) {
        if (usedWords[i][1] === startRowIndex && usedWords[i][2] === startColIndex && usedWords[i][3] === endRowIndex && usedWords[i][4] === endColIndex) {
          $('.marked').addClass('found');  //mark the letters of the word on the game board
          $("#" + usedWords[i][0]).css('color', 'gray');  //change the color of the hint word
          calculateScore(usedWords[i][5]);
          usedWords.splice(i, 1);  //remove the word from used words list
          $('.current-score p').text(score);  //update the score
          isEnd();  //check if it's the end of the level
        }
      }
      $('td').removeClass('marked');
    });

    $('.gameboard').on('mouseup touchend', function() {
      //no more marking
      $('td').off('mouseover touchmove');
    });
  }

// a function to mark the cells that make a streight line from the start of marking to the end
  function markCorrectCells() {
    // marking: moving on the X axis mainly
    if (Math.abs(currColIndex - startColIndex) >= Math.abs(currRowIndex - startRowIndex)) {
      // moving from right to left
      if (startColIndex > currColIndex) {
        for (var i = currColIndex; i <= startColIndex; i++) {
          $('table tr').eq(startRowIndex).find('td').eq(i).addClass('marked');
        }
      }
    // moving from left to right
     if (startColIndex < currColIndex) {
       for (var i = startColIndex; i <= currColIndex; i++) {
         $('table tr').eq(startRowIndex).find('td').eq(i).addClass('marked');
       }
     }
     endColIndex = currColIndex;
     endRowIndex = startRowIndex;
   }
     // marking: moving on the Y axis mainly
     if (Math.abs(currColIndex - startColIndex) < Math.abs(currRowIndex - startRowIndex)) {
       // moving up
       if (startRowIndex > currRowIndex) {
         for (var i = currRowIndex; i <= startRowIndex; i++) {
           $('table tr').eq(i).find('td').eq(startColIndex).addClass('marked');
         }
       }
     // moving down
      if (startRowIndex < currRowIndex) {
        for (var i = startRowIndex; i <= currRowIndex; i++) {
          $('table tr').eq(i).find('td').eq(startColIndex).addClass('marked');
        }
      }
      endColIndex = startColIndex;
      endRowIndex = currRowIndex;
    }
  }

//Result of hitting the "next level" button - next levels is loaded.
  $('.end button').on('click', function() {
    if (startingGame) {
      $('.end').hide();
    } else {
      level += 1;
      initialize();
    }
  });

//Result of hitting the "choose level" button - a selection of levels is loaded.
  $('#choose-level').on('click', function() {
    var chooseLevelPosition = $('#choose-level').position();  // rippmenüü õigesse kohta
    $('.levels').css('top', (chooseLevelPosition.top + 80) + 'px');
    $('.levels').css('left', (chooseLevelPosition.left) + 'px');
    $('.levels').toggle();
  });

  //Result of selecting level 1 - a new game on level 0 is loaded.
    $('#level-0').on('click', function() {
      level = 0;
      $('.gameboard img').remove();
      initialize();
      $('.levels').hide();
    });

  //Result of selecting level 2 - a new game on level 1 is loaded.
    $('#level-1').on('click', function() {
      if (level1) {
        level = 1;
        $('.gameboard img').remove();
        initialize();
        $('.levels').hide();
      }
    });

  //Result of selecting level 3 - a new game on level 2 is loaded.
    $('#level-2').on('click', function() {
      if (level2) {
        level = 2;
        $('.gameboard img').remove();
        initialize();
        $('.levels').hide();
      }
    });

  //Result of selecting level 4 - a new game on level 3 is loaded.
    $('#level-3').on('click', function() {
      if (level3) {
        level = 3;
        $('.gameboard img').remove();
        initialize();
        $('.levels').hide();
      }
    });

  //Result of selecting level 5 - a new game on level 4 is loaded.
    $('#level-4').on('click', function() {
      if (level4) {
        level = 4;
        $('.gameboard img').remove();
        initialize();
        $('.levels').hide();
      }
    });

  //Result of selecting level 6 - a new game on level 5 is loaded.
    $('#level-5').on('click', function() {
      if (level5) {
        level = 5;
        $('.gameboard img').remove();
        initialize();
        $('.levels').hide();
      }
    });

  //Result of selecting level 7 - a new game on level 6 is loaded.
    $('#level-6').on('click', function() {
      if (level6) {
        level = 6;
        $('.gameboard img').remove();
        initialize();
        $('.levels').hide();
      }
    });

//Calculates the current score.
  function calculateScore(wordLen) {
    newTime = new Date().getTime();
    score += Math.floor(20000 / (newTime - currTime) * wordLen * (level + 1));
    currTime = newTime;
  }

//makes faces appear on the screen
  function flyFace(waitTime, imgId) {
    var fromTop = Math.floor(Math.random() * 50) + 10;  //y coordinate
    var fromLeft = Math.floor(Math.random() * 60) + 10;  //x coordinate
    var happyOrSad = Math.floor(Math.random() * 2);  //which emotion is displayed
    var nrOfPic = Math.floor(Math.random() * 21) + 1;  //nr of pic from the folder
    if (happyOrSad === 0) {
      var fileName = "fotod/happy/small/happy" + nrOfPic + ".jpg";
      happyFace = true;
      //add the image with a unique class
      $('.gameboard').prepend('<img class="' + imgId + '" src=' + fileName + ' />');
      //change the coordinates of the face image to random
      $('.' + imgId).css('top', fromTop + '%');
      $('.' + imgId).css('left', fromLeft + '%');
      //hide and show and hide again
      $('.' + imgId).hide().delay(waitTime).fadeIn(500).delay(1500).fadeOut(500);
    //Result of clicking a flying face - adds to score if the face is happy, subtracts, it its not.
      $('.' + imgId).bind('click', function() {
        score += 100;
        $(this).remove();
        $('.current-score p').text(score);
      });
    } else {
      var fileName = "fotod/neg/small/neg" + nrOfPic + ".jpg";
      happyFace = false;
      //add the image with a unique class
      $('.gameboard').prepend('<img class="' + imgId + '" src=' + fileName + ' />');
      //change the coordinates of the face image to random
      $('.' + imgId).css('top', fromTop + '%');
      $('.' + imgId).css('left', fromLeft + '%');
      //hide and show and hide again
      $('.' + imgId).hide().delay(waitTime).fadeIn(500).delay(1500).fadeOut(500);
    //Result of clicking a flying face - adds to score if the face is happy, subtracts, it its not.
      $('.' + imgId).bind('click', function() {
        score -= 20;
        $(this).remove();
        $('.current-score p').text(score);
      });
    }
  $('.' + imgId).on('dragstart', function(event) { event.preventDefault(); });
  }

//Checks if the level has ended and then displays a congratulation text.
  function isEnd() {
    if (usedWords.length < 1) {
      totalScore += score;
      $('.total-score p').text(totalScore);
      startingGame = false;
      $("#final-score").text(""); //eemaldab üleliigse teksti
      $('#good-job').show();
      $('.end button').text("Järgmine tase");
      $('.gameboard img').remove();
      $('.end').fadeIn(500);
      $("#final-score").append("Said selle mängu eest " + score + " punkti.")
      if (level === 0 && !level1) {
        $("#level-info").append(" Järgmisel tasemel ilmuvad mängulauale aeg-ajalt fotod inimeste nägudega. Mängus on võimalik teenida lisapunkte, kui klikkad piltidel, millel on rõõmsad ja õnnelikud näod. Kuid pane tähele - teistel piltidel klikkimine viib punkte.")
      } else {
        $("#level-info").text("Kokku on Sul " + totalScore + " punkti.");
      }
    }
  }

  initialize();

}

$(document).ready(main);
